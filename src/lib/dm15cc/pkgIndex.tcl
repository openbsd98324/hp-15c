if {![package vsatisfies [package provide Tcl] 8.5]} {return}
package ifneeded dm15cc 1.9.4 [list source [file join $dir dm15cc.tcl]]
package ifneeded DM15 2.0.0 [list source [file join $dir DM15.tcl]]