if {![package vsatisfies [package provide Tcl] 8.2]} {return}
package ifneeded html 1.4 [list source [file join $dir html.tcl]]
package ifneeded htmlTagAttr 1.0 [list source [file join $dir htmlTagAttr.tcl]]
