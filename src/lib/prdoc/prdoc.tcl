# ------------------------------------------------------------------------------
#
#            Program Dcoumentation Package for the HP-15C Simulator
#
#                          (c) 2017-2022 Torsten Manz
#
# ------------------------------------------------------------------------------
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, see <https://www.gnu.org/licenses/>
#
# ------------------------------------------------------------------------------

package require Tcl 8
package require math
package require htmlTagAttr
package provide prdoc 1.9.7

namespace eval ::prdoc {

  variable geomRE {=?((\d+)x(\d+))?([+-])(-?\d+)([+-])(-?\d+)}
  variable CONF
  array set CONF {
    autopreview 1
    unicodesyms 1
    ShowResTab 0
    taghighlight 1
    tagcolour #800080
    tagbold 1
  }

# Bindings for Usage text field
  variable ADDTAG_BINDS
  array set ADDTAG_BINDS {
    a         {ctrl shift A}
    code      {ctrl shift C}
    em        {ctrl shift I}
    fKey      {ctrl opt f}
    fKeyLabel {ctrl shift F}
    gKey      {ctrl opt g}
    gKeyLabel {ctrl shift G}
    KeyLabel  {ctrl shift K}
    li        {ctrl shift L}
    ol        {ctrl shift O}
    pre       {ctrl shift P}
    register  {ctrl shift R}
    strong    {ctrl shift B}
    sup       {ctrl shift S}
    sub       {ctrl shift T}
    ul        {ctrl shift U}
  }

  variable ADDCHAR_BINDS
  array set ADDCHAR_BINDS {
    emdash  {ctrl underscore  "\u2014"}
    rarr    {ctrl greater     "\u2192"}
    larr    {ctrl less        "\u2190"}
    middot1 {ctrl asterisk    "\u00B7"}
    middot2 {ctrl KP_Multiply "\u00B7"}
  }

# OS specific settings
  variable LAYOUT
  variable MODKEY

  if {[tk windowingsystem] eq "aqua"} {
    set LAYOUT(btnwid) 3
    set LAYOUT(tabpad) [list 0 8 0 3]

    array set MODKEY {
      ctrl "Command"
      opt "Option"
      shift "Shift"
    }
    array set ADDTAG_BINDS {
      a  {ctrl opt a}
      li {ctrl opt l}
    }

    array unset ADDCHAR_BINDS ?arr

  } else {
    set LAYOUT(btnwid) 4
    set LAYOUT(tabpad) [list 8 8 6 3]

    array set MODKEY {
      ctrl "Control"
      opt "Alt"
      shift "Shift"
    }
  }

# Description variables
  variable DESC
  array set DESC {}
  variable DESC_T
  array set DESC_T {}
  variable Dchanged 0

  variable MARS
  array set MARKS { L {} R {} F {} }

# Fonts and styles for HTML rendering
  variable prevpos ""

  set basesize [font actual TkTextFont -size]
# WA-Linux: "font actual" returns "-size 0" for Tk standard fonts
  if {$basesize <= 0} {
    set basesize [expr int([font metrics TkFixedFont -linespace]*0.65)]
  }
  foreach {tt fs wt} {h1 6 bold h2 4 bold h3 3 bold h4 2 bold h5 1 bold \
    h6 0 bold strong 0 bold em 0 normal reg 0 bold keyface 1 bold \
    sub -1 normal} {
    font create FnPrDoc$tt -family [font actual TkTextFont -family] \
      -size [expr $basesize+$fs] -weight $wt
  }
  font configure FnPrDocem -slant italic
  font configure FnPrDocstrong -weight bold
  font configure FnPrDocreg -family Times -weight bold -size [expr int($basesize*1.2)]
  font create FnFixstrong -family [font actual TkFixedFont -family] \
    -size $basesize -weight bold
  font create FnFixem -family [font actual TkFixedFont -family] \
    -size $basesize -weight normal -slant italic

  array set TagStyle {
    h1 {{<h1>} {</h1>} {-font FnPrDoch1}}
    h2 {{<h2>} {</h2>} {-font FnPrDoch2}}
    h3 {{<h3>} {</h3>} {-font FnPrDoch3}}
    h4 {{<h4>} {</h4>} {-font FnPrDoch4}}
    h5 {{<h5>} {</h5>} {-font FnPrDoch5}}
    h6 {{<h6>} {</h6>} {-font FnPrDoch6}}
    bold {{<b>} {</b>} {-font FnPrDocstrong}}
    strong {{<strong>} {</strong>} {-font FnPrDocstrong}}
    i {{<i>} {</i>} {-font FnPrDocem}}
    em {{<em>} {</em>} {-font FnPrDocem}}
    pre {{<pre>} {</pre>} {-font TkFixedFont -back #F0F0F0}}
    code {{<code>} {</code>} {-font TkFixedFont}}
    ol {{<ol>} {</ol>} {}}
    ul {{<ul>} {</ul>} {}}
    li {{<li>} {</li>} {}}
    a {{<a[^>]+>} {</a>} {-fore blue}}
    KeyLabel {{<span class="HP15CKey">} {</span>} \
      {-font FnPrDockeyface -fore white -back #454545}}
    fKeyLabel {{<span class="HP15CfKeyLabel">} \
      {</span>} {-font FnPrDockeyface -fore #E1A83E -back #454545}}
    gKeyLabel {{<span class="HP15CgKeyLabel">} \
      {</span>} {-font FnPrDockeyface -fore #6CB7BD -back #454545}}
    fKey {{<span class="HP15CfKey">} {</span>} \
      {-font FnPrDockeyface -fore black -back #E1A83E}}
    gKey {{<span class="HP15CgKey">} {</span>} \
      {-font FnPrDockeyface -fore black -back #6CB7BD}}
    register {{<span class="HP15CRegister">} {</span>} {-font FnPrDocreg}}
    noimage {{<img[^>]+} {>} {-font FnPrDocstrong \
      -back #F0F0F0  -justify center -lmargin1 30 -rmargin 30}}
  }
  set TagStyle(sup) [list <sup> </sup> [list -font FnPrDocsub -offset [expr $basesize/2]]]
  set TagStyle(sub) [list <sub> </sub> [list -font FnPrDocsub -offset [expr -$basesize/2]]]

  ttk::style configure strong.TButton -font FnPrDocstrong -width 3
  ttk::style configure em.TButton -font FnPrDocem -width 3
  ttk::style configure pre.TButton -font TkFixedFont -width 4
  ttk::style configure reg.TButton -font FnPrDocreg -width 4
  ttk::style configure tag.TButton -width 4
  ttk::style configure gold.TButton -font FnPrDocstrong -foreground #E1A83E -width 4
  ttk::style configure blue.TButton -font FnPrDocstrong -foreground #6CB7BD -width 4
  ttk::style configure fkey.TButton -font FnPrDocstrong -foreground #E1A83E -width 3
  ttk::style configure gkey.TButton -font FnPrDocstrong -foreground #6CB7BD -width 3

# WA-macOS: The standard button padding is much to wide
  if {[tk windowingsystem] eq "aqua"} {
    foreach st [list strong em pre reg tag gold blue fkey gkey] {
      ttk::style configure $st.TButton -padding -15
    }
  }

  variable Symbols
  set Symbols(greek) {
    &Alpha; \u0391 &Beta; \u0392 &Gamma; \u0393 &Delta; \u0394 Epsilon; \u0395
    &Zeta; \u0396 &Eta; \u0397 &Theta; \u0398 &Iota; \u0399 &Kappa; \u039a
    &Lambda; \u039b &Mu; \u039c &Nu; \u039d &Xi; \u039e &Omicron; \u039f
    &Pi; \u03a0 &Rho; \u03a1 &Sigma; \u03a3 &Tau; \u03a4 &Upsilon; \u03a5
    &Phi; \u03a6 &Chi; \u03a7 &Psi; \u03a8 &Omega; \u03a9 &alpha; \u03b1
    &beta; \u03b2 &gamma; \u03b3 &delta; \u03b4 &epsilon; \u03b5 &zeta; \u03b6
    &eta; \u03b7 &theta; \u03b8 &iota; \u03b9 &kappa; \u03ba &lambda; \u03bb
    &mu; \u03bc &nu; \u03bd &xi; \u03be &omicron; \u03bf &pi; \u03c0 &rho; \u03c1
    &sigma; \u03c3 &tau; \u03c4 &upsilon; \u03c5 &phi; \u03c6
    &chi; \u03c7 &psi; \u03c8 &omega; \u03c9 &thetasym; \u03d1 &piv; \u03d6 \
    &sigmaf; \u03c2
  }

  set Symbols(arrows) {
    &larr; \u2190 &uarr; \u2191 &rarr; \u2192 &darr; \u2193 &harr; \u2194
    &crarr; \u21b5 &lArr; \u21d0 &uArr; \u21d1 &rArr; \u21d2 &dArr; \u21d3
    &hArr; \u21d4
  }

  set Symbols(math) {
    &divide; \u00f7 &frasl; \u2044 &times; \u00d7 &minus; \u2212 &plusmn; \u00b1
    &not; \u00ac &weierp; \u2118 &image; \u2111
    &real; \u211c &trade; \u2122 &alefsym; \u2135 &forall; \u2200
    &part; \u2202 &exist; \u2203 &empty; \u2205 &nabla; \u2207 &isin; \u2208
    &notin; \u2209 &ni; \u220b &prod; \u220f &sum; \u2211
    &lowast; \u2217 &radic; \u221a &prop; \u221d &infin; \u221e &ang; \u2220
    &and; \u2227 &or; \u2228 &cap; \u2229 &cup; \u222a &int; \u222b \
    &there4; \u2234 &sim; \u223c &cong; \u2245 &asymp; \u2248 &ne; \u2260 \
    &equiv; \u2261 &lt; \u003c &gt; \u003e &le; \u2264 &ge; \u2265 &sub; \u2282 \
    &sup; \u2283  &nsub; \u2284 &sube; \u2286 &supe; \u2287 &oplus; \u2295 \
    &otimes; \u2297 &perp; \u22a5 &mitdot; \u00B7 &sdot; \u22c5 &lceil; \u2308 \
    &rceil; \u2309 &lfloor; \u230a &rfloor; \u230b &lang; \u2329 &rang; \u232a \
    &loz; \u25ca
  }

  set Symbols(moresyms) {
    &euro; \u20ac &cent; \u00a2 &pound; \u00a3 &yen; \u00a5 &copy; \u00a9
    &reg; \u00ae &ordf; \u00aa &ordm; \u00ba &laquo; \u00ab &raquo; \u00bb
    &sup1; \u00b9 &sup2; \u00b2 &sup3; \u00b3 &micro; \u00b5 &para; \u00b6
    &frac14; \u00bc &frac12; \u00bd &frac34; \u00be &fnof; \u0192
    &bull; \u2022 &spades; \u2660 &clubs; \u2663 &hearts; \u2665 &diams; \u2666
    &hellip; \u2026 &amp; \u0026 &brvbar; \u00a6 &tilde; \u02dc &ndash; \u2013
    &mdash; \u2014 &permil; \u2030
  }

  set Symbols(other) {
    &deg; \u00b0 &nbsp; \u00a0 &iexcl; \u00a1 &curren; \u00a4 &sect; \u00a7
    &uml; \u00a8 &Agrave; \u00c0 &Aacute; \u00c1 &Acirc; \u00c2 &Atilde; \u00c3
    &Auml; \u00c4 &Aring; \u00c5 &AElig; \u00c6 &Ccedil; \u00c7 &Egrave; \u00c8
    &Eacute; \u00c9 &Ecirc; \u00ca &Euml; \u00cb &Igrave; \u00cc &Iacute; \u00cd
    &Icirc; \u00ce &Iuml; \u00cf &ETH; \u00d0 &Ntilde; \u00d1 &Ograve; \u00d2
    &Oacute; \u00d3 &Ocirc; \u00d4 &Otilde; \u00d5 &Ouml; \u00d6 &Oslash; \u00d8
    &Ugrave; \u00d9 &Uacute; \u00da &Ucirc; \u00db &Uuml; \u00dc &Yacute; \u00dd
    &szlig; \u00df &agrave; \u00e0 &aacute; \u00e1 &acirc; \u00e2 &atilde; \u00e3
    &auml; \u00e4 &aring; \u00e5 &aelig; \u00e6 &ccedil; \u00e7 &egrave; \u00e8
    &eacute; \u00e9 &ecirc; \u00ea &euml; \u00eb &igrave; \u00ec &iacute; \u00ed
    &icirc; \u00ee &iuml; \u00ef &eth; \u00f0 &ntilde; \u00f1 &ograve; \u00f2
    &oacute; \u00f3 &ocirc; \u00f4 &otilde; \u00f5 &ouml; \u00f6 &oslash; \u00f8
    &ugrave; \u00f9 &uacute; \u00fa &ucirc; \u00fb &uuml; \u00fc &yacute; \u00fd
    &thorn; \u00fe &THORN; \u00de &yuml; \u00ff &OElig; \u0152 &oelig; \u0153
    &Scaron; \u0160 &scaron; \u0161 &Yuml; \u0178 &shy; \u00ad &macr; \u00af
    &ensp; \u2002 &emsp; \u2003 &thinsp; \u2009 &zwnj; \u200c &zwj; \u200d
    &lrm; \u200e &rlm; \u200f &acute; \u00b4 &middot; \u00b7 &cedil; \u00b8
    &iquest; \u00bf &upsih; \u03d2 &prime; \u2032 &Prime; \u2033 &oline; \u203e
    &quot; \u0022 &circ; \u02c6 &lsquo; \u2018 &rsquo; \u2019 &sbquo; \u201a
    &ldquo; \u201c &rdquo; \u201d &bdquo; \u201e &dagger; \u2020 &Dagger; \u2021
    &lsaquo; \u2039 &rsaquo; \u203a
  }

  variable HTMLentities
  set HTMLentities [concat $Symbols(greek) $Symbols(arrows) $Symbols(math) \
    $Symbols(moresyms) $Symbols(other)]

}

# ------------------------------------------------------------------------------
proc ::prdoc::Analyse { prgm } {

  variable MARKS
  array set MARKS { L {} R {} F {} }

  set aregs {}
  set altmp {}
  set nltmp {}

# Scan the program for Labels, Register and Flags
  foreach pl $prgm {
    if {[regexp {4[45]_([1234]0_)*(48_)*([0-9])$} $pl \
      step oper dec reg]} {
      if {$dec ne ""} {incr reg 10}
      lappend MARKS(R) $reg
    } elseif {[regexp {4[45]_([1234]0_)*24$} $pl step]} {
      lappend aregs "(i)"
    } elseif {[regexp {4[45]_([1234]0_)*25$} $pl step]} {
      lappend aregs "I"
    } elseif {[regexp {[23]2_1([1-5])$} $pl step lbl]} {
      lappend altmp -$lbl
    } elseif {[regexp {[23]2_(48_)*([0-9])$} $pl step dec lbl]} {
      if {$dec ne ""} {incr lbl 10}
      lappend nltmp $lbl
    } elseif {[regexp {42_21_(48_)*([0-9])$} $pl step dec lbl]} {
      if {$dec ne ""} {incr lbl 10}
      lappend nltmp $lbl
    } elseif {[regexp {42_21_1([1-5])$} $pl step lbl]} {
      lappend altmp -$lbl
    }
  }
  set MARKS(L) [concat \
    [lsort -unique -integer -decreasing $altmp] [lsort -unique -integer $nltmp]]

  set MARKS(R) [lsort -unique -integer $MARKS(R)]
  lappend MARKS(R) {*}[join [lsort -unique -dictionary $aregs]]

  foreach pl $prgm {
    if {[regexp {43_[456]_([0-9])} $pl ign flag]} {
      lappend MARKS(F) $flag
    }
  }
  set MARKS(F) [lsort -unique -integer $MARKS(F)]

}

# ------------------------------------------------------------------------------
proc ::prdoc::Reload {} {

  if {[winfo exists .pdocu]} {
    set geom "+[winfo x .pdocu]+[winfo y .pdocu]"
    destroy .pdocu
    Edit $geom
  }

}

# ------------------------------------------------------------------------------
proc ::prdoc::ReDraw {} {

  if {[winfo exists .pdocu]} {
    set geom "[winfo geometry .pdocu]"
    destroy .pdocu
    Draw $geom
  }

}

# ------------------------------------------------------------------------------
proc ::prdoc::SaveDesc {} {

  variable DESC
  variable DESC_T

  foreach nn [array names DESC_T] {
    set DESC($nn) [string trim $DESC_T($nn)]
  }
  set DESC(D) \
    [regsub -all { +\n} [.pdocu.description.edit.txt get 0.0 end] "\n"]
  set DESC(D) [regsub {\n*$} $DESC(D) ""]

}

# ------------------------------------------------------------------------------
proc ::prdoc::Return { wid } {

  if {[winfo class $wid] ne "Text"} {
    Act yes
  }

}

# ------------------------------------------------------------------------------
proc ::prdoc::Act { action }  {

  variable geomRE
  variable Dchanged
  variable prevpos

  set rc true

  if {![winfo exists .pdocu]} {return $rc}

  regexp $geomRE [winfo geometry .pdocu] all w1xh1 w1 h1 xoff x1 yoff y1
  set prevpos $xoff$x1$yoff$y1

  if {$Dchanged && $action ne "yes"} {
    wm deiconify .pdocu
    focus .pdocu
    set action [tk_messageBox -parent .pdocu -icon question -type yesnocancel \
      -default yes -title [mc menu.prgmdocu] -message [mc pdocu.changed]]
  }

  switch $action {
    "yes" {
      SaveDesc
      destroy .pdocu
    }
    "no" {
      destroy .pdocu
    }
    "cancel" {
      wm deiconify .pdocu
      focus .pdocu
      set rc false
    }
  }

  return $rc

}

# ------------------------------------------------------------------------------
proc ::prdoc::Purge { prgm } {

  variable DESC
  variable MARKS

  Analyse $prgm
  foreach pd [array names DESC -glob {[LRF]*}] {
    set mm [string index $pd 0]
    if {[lsearch $MARKS($mm) [string range $pd 1 end]] < 0} {
      array unset DESC $pd
    }
  }

}

# ------------------------------------------------------------------------------
proc ::prdoc::Changed { wid {n1 ""} {n2 ""} {op ""} } {

  variable Dchanged

  if {!$Dchanged} {
    set Dchanged 1
    set tlw [winfo toplevel $wid]
    wm title $tlw "[wm title $tlw] *"
  }

}

# ------------------------------------------------------------------------------
proc ::prdoc::Discard {}  {

  variable Dchanged
  set rc true

  if {![winfo exists .pdocu] || !$Dchanged} {return $rc}

  wm deiconify .pdocu
  focus .pdocu
  set answer [tk_messageBox -parent .pdocu -icon question -type yesno \
    -default yes -title [mc menu.prgmdocu] -message [mc pdocu.discard]]
  if {$answer eq "no"} { set rc false }

  return $rc

}

# ------------------------------------------------------------------------------
proc ::prdoc::Modified { wid } {

  if {[$wid edit modified]} {
    HighlightTags $wid
    Changed $wid
  }

}

# ------------------------------------------------------------------------------
proc ::prdoc::ResetModified { wid } {

  $wid edit modified 0

}

# ------------------------------------------------------------------------------
proc ::prdoc::RenderImage { wid } {

  set count ""
  set idict [dict create]

  set res [$wid search -forwards -regexp -nocase -all -count count -nolinestop \
    {<img[^>]+>} 1.0 end]
  if {[llength $res] == 0 || [llength $count] == 0} { return }

  foreach rr $res cc $count {
    ::htmlTagAttr::img [$wid get $rr "$rr + $cc chars"] idict

    if {[dict exists $idict src]} {
      set iext [file extension [dict get $idict src]]
      set fnam "$::HP15(prgmdir)/[dict get $idict src]"

      if {[file isfile $fnam] && [string tolower $iext] in {".gif" ".png"}} {
        $wid image create $rr -image [image create photo -file $fnam]
      } else {
        if {[dict exists $idict alt]} {
          set talt [dict get $idict alt]
        } {
          set talt [dict get $idict src]
        }
        if {[file isfile $fnam]} {
          append talt "\n([mc pdocu.previewfmt])"
        } else {
          append talt "\n([mc pdocu.noimagefile])"
        }
        $wid replace $rr "$rr + $cc chars" "\n$talt\n\n" tagnoimage
      }
    }
  }

}

# ------------------------------------------------------------------------------
proc ::prdoc::RenderPre { wid } {

  set ii 0

  set prelst [$wid search -all -regexp -nolinestop {<pre>} 0.0 end]
  foreach p1 $prelst {
    set p2 [$wid search -regexp -nolinestop {</pre>} $p1 end]

    foreach {tt fn} {em FnFixem i FnFixem strong FnFixstrong} {
      set ttlst [$wid search -all -regexp -nolinestop "<$tt>(.*?)</$tt>" $p1 $p2]
      foreach t1 $ttlst {
        set t2 [$wid search -regexp -nolinestop "</$tt>" $t1 end]
        $wid tag add pretag$ii $t1 $t2
        $wid tag configure pretag$ii -font $fn
      }
      incr ii
    }
  }

}

# ------------------------------------------------------------------------------
proc ::prdoc::Render { wid } {

  variable TagStyle
  variable HTMLentities

# Padding for HP-15C CSS styles content
  set dd [$wid get 0.0 end]
  foreach tt [list KeyLabel fKeyLabel gKeyLabel fKey gKey] {
    lassign $TagStyle($tt) topen tclose topts
    set tpattern "($topen)(.*?)($tclose)"
    regsub -all -nocase $tpattern $dd {\1 \2 \3} dd
  }

# Replace individual tags and characters
  regsub -all -nocase "<br>" $dd "\n" dd
  regsub -all -nocase "<p>" $dd "\n" dd

# Replace HTML Entities
  set dd [string map $HTMLentities $dd]

# Format lists
  set olcnt {}
  set trng [regexp -nocase -indices -inline {<ol>|<ul>} $dd]
  while {[llength [lindex $trng 0]] > 0} {
    lassign [lindex $trng 0] p0 p1
    set p2 $p1
    switch [string range $dd $p0 $p1] {
      "<ol>" {
        lappend olcnt ol 0
      }
      "<ul>" {
        lappend olcnt ul 0
      }
      "</ol>" -
      "</ul>" {
        set olcnt [lrange $olcnt 0 end-2]
      }
      "<li>" {
        set lipad [string repeat "  " [llength $olcnt]]
        incr p2 [string length $lipad]
        switch [lindex $olcnt end-1] {
          "ol" {
            lset olcnt end [expr [lindex $olcnt end] + 1]
            set dd [string replace $dd $p0 $p1 "$lipad<li>[lindex $olcnt end]. "]
          }
          "ul" {
            set dd [string replace $dd $p0 $p1 "$lipad<li>\u2022 "]
          }
        }
      }
    }
    set trng [regexp -nocase -indices -inline -start $p2 {</*[ou]l>|<li>} $dd]
  }

# Replace render widget content with updated text
  $wid replace 0.0 end $dd

# Tagging
  foreach tt [array names TagStyle] {
    lassign $TagStyle($tt) topen tclose topts
    set tpattern "$topen.*?$tclose"
    set count {}
    set res [$wid search -forwards -regexp -nocase -all -count count -nolinestop \
      $tpattern 1.0 end]
    if {[llength $res] > 0 && [llength $count] > 0} {
      foreach rr $res cc $count {
        $wid tag add tag$tt $rr "$rr + $cc chars"
      }
    }
    $wid tag configure tag$tt {*}$topts
  }

# Render margins
  set bgcol [$wid cget -background]
  $wid tag configure tagpre -lmargin1 20 -lmargincolor $bgcol \
    -rmargin 20 -rmargincolor $bgcol
  $wid tag configure tagnoimage -lmargincolor $bgcol -rmargincolor $bgcol

# Render images
  ::prdoc::RenderImage $wid

  ::prdoc::RenderPre $wid

# Make links clickable
  set res [$wid search -forwards -regexp -nocase -all -count count \
    "<a href.*</a>" 1.0 end]
  if {[llength $res] > 0 && [llength $count] > 0} {
    set ii 0
    foreach rr $res cc $count {
      regexp -nocase {<a href="([^"]+)".*>} [$wid get $rr "$rr + $cc chars"] ign url
      if {[info exists url] && $url != ""} {
        $wid tag add url$ii $rr "$rr + $cc chars"
        $wid tag bind url$ii <Button-1> "url_open $url"
        $wid tag bind url$ii <Enter> "$wid configure -cursor hand2"
        $wid tag bind url$ii <Leave> "$wid configure -cursor arrow"
      }
      incr ii
    }
  }

# Hide tags
  set res [$wid search -forwards -regexp -nocase -all -count count \
    "</*\[\[:alnum:]]+\[^>]*>" 1.0 end]
  foreach rr $res cc $count {
    $wid tag add tagtag $rr "$rr + $cc chars"
  }
  $wid tag configure tagtag -elide true

}

# ------------------------------------------------------------------------------
proc ::prdoc::SetMode { mode } {

  variable CONF

  if {$mode eq "render"} {
    set wid .pdocu.description.render

    $wid.txt configure -state normal
    $wid.txt replace 0.0 end [.pdocu.description.edit.txt get 0.0 end]
    prdoc::Render $wid.txt
    $wid.txt configure -state disabled

    bind .pdocu <F6> "::prdoc::SetMode edit"
    raise $wid
    focus .pdocu
  } else {
    set wid .pdocu.description.edit
    if {$CONF(taghighlight)} "HighlightTags $wid.txt"

    bind .pdocu <F6> "::prdoc::SetMode render"
    raise $wid
    focus $wid.txt
  }

}

# ------------------------------------------------------------------------------
proc ::prdoc::AddChar { wid val } {

  set sel [$wid tag ranges sel]
  if {[llength $sel] == 0} {
    $wid insert [$wid index insert] $val
  } else {
    set mark [$wid get [lindex $sel 0] [lindex $sel 1]]
    $wid replace [lindex $sel 0] [lindex $sel 1] "$val"
  }

  HighlightTags $wid
  focus $wid

}

# ------------------------------------------------------------------------------
proc ::prdoc::AddTag { wid tag } {

  variable TagStyle

  lassign $TagStyle($tag) topen tclose ign
  set sel [$wid tag ranges sel]
  if {[llength $sel] == 0} {
    $wid insert [$wid index insert] "$topen$tclose"
    $wid mark set insert "[$wid index insert] - [string length $tclose]c"
  } else {
    set mark [$wid get [lindex $sel 0] [lindex $sel 1]]
    if {[regexp "^$topen\(.*)$tclose$" $mark mv smv1]} {
      $wid replace [lindex $sel 0] [lindex $sel 1] $smv1
    } else {
      $wid insert [lindex $sel 1] $tclose
      if {$tag eq "a"} {
        set url ""
        regexp -nocase "^https*://.*" [$wid get sel.first sel.last] url
        set topen "<a href=\"$url\">"
      }
      $wid insert [lindex $sel 0] $topen
      $wid mark set insert "[lindex $sel 1] + [string length $topen]c"
    }
  }
  HighlightTags $wid
  focus $wid

}

# ------------------------------------------------------------------------------
proc ::prdoc::HighlightTags { wid } {

  variable CONF

  $wid tag delete tagtag
  if {$CONF(taghighlight)} {
    set count {}
    set res [$wid search -forwards -regexp -nocase -all -count count \
      "</*\[\[:alnum:]]+\[^>]*>" 1.0 end]
    foreach rr $res cc $count {
      $wid tag add tagtag $rr "$rr + $cc chars"
    }
    $wid tag configure tagtag -foreground $CONF(tagcolour)
    if {$CONF(tagbold)} {
      $wid tag configure tagtag -font FnPrDocstrong
    }
  } else {
    $wid tag configure tagtag -foreground black -font TkTextFont
  }

}

# ------------------------------------------------------------------------------
proc ::prdoc::SymbolsMenu { wid ewid } {

  variable Symbols
  variable CONF

  if {[winfo exists $wid]} {destroy $wid}
  menu $wid

  foreach {nn bb} [list greek 12 math 10 arrows 6 moresyms 8] {
    set mm $wid.$nn
    menu $mm
    $wid add cascade -label [mc gen.$nn] -menu $mm
    set ii 0
    foreach {hent ucode} $Symbols($nn) {
      if {$CONF(unicodesyms) && !($hent in [list "&lt;" "&gt;"])} {
        set hent $ucode
      }
      $mm add command -label $ucode -command "::prdoc::AddChar $ewid \"$hent\""
      if {$ii % $bb == 0} { $mm entryconfigure $ii -columnbreak 1 }
      incr ii
    }
  }

}

# ------------------------------------------------------------------------------
proc ::prdoc::DrawMarks { wid } {

  variable LAYOUT
  variable CONF
  variable MARKS

  set lrf [expr \
   [llength $MARKS(L)]+[llength $MARKS(R)]+[llength $MARKS(F)]]
  if {[winfo screenheight .] < 801 || $lrf > 30} {
    set colcnt 3.0
  } else {
    set colcnt 2.0
  }

  set heightsav [winfo height [winfo toplevel $wid]]

  set fpm $wid.marks
  set redraw [winfo exists $fpm]
  catch {destroy $fpm}

# Tabbed?
  if {$CONF(ShowResTab)} {
    ttk::notebook $fpm -padding [list 0 6 0 0]
  } else {
    ttk::frame $fpm
  }
  grid $fpm -row 1 -column 0 -padx 3 -sticky nsew

  set row 1
  foreach {nam idx txt} {lbl L labels regs R regs flags F flags} {
    if {$CONF(ShowResTab)} {
      set fnam $fpm.$nam
    } else {
      ttk::labelframe $fpm.$nam -relief groove -borderwidth 2 -text " [mc gen.$txt] "
      grid columnconfigure $fpm.$nam 0 -weight 1
      set fnam $fpm.$nam.if
    }
    ttk::frame $fnam

    set rr 0
    set cc 0
    foreach ll $MARKS($idx) {
      ttk::label $fnam.label$ll -text "[format_mark $ll] " -width 3 -anchor e
      ttk::entry $fnam.value$ll -width 40 -textvariable prdoc::DESC_T($idx$ll)
      grid $fnam.label$ll -row $rr -column $cc -sticky e -pady 1
      grid $fnam.value$ll -row $rr -column [expr $cc+1] -sticky we -pady 1
      incr rr
      if {$rr > int(ceil([llength $MARKS($idx)]/$colcnt))-1} {
        set rr 0
        incr cc 2
      }
    }
    if {[llength $MARKS($idx)] == 0} {
      ttk::label $fnam.nolabels -text "[mc pdocu.no$txt]" -anchor w -justify left
      grid $fnam.nolabels -row 0 -column 0 -padx 3 -pady 1 -sticky w
    }
    if {[llength $MARKS($idx)] == 1 || $colcnt == 1} {
      grid columnconfigure $fnam {1} -weight 2
    } else {
      for {set ii 1} {$ii < $colcnt*2} {incr ii 2} {
        grid columnconfigure $fnam $ii -weight 2
      }
    }
    grid $fnam -row 0 -column 0 -padx 7 -pady 5 -sticky nwse

  # Lay out marks
    if {$CONF(ShowResTab)} {
      $fnam configure -padding $LAYOUT(tabpad)
      $fpm add $fnam -text " [mc gen.$txt] " -sticky nsew
    } else {
      grid $fpm.$nam -row $row -column 0 -sticky nsew
    }
    incr row
  }

  grid columnconfigure $fpm 0 -weight 1
  grid $fpm -row 1 -column 0 -sticky nsew

  if {$redraw} {
    update
    set heightnew [expr max(int(2.5*[winfo height $fpm]), $heightsav)]
    wm geometry $wid [winfo width $wid]x$heightnew
  }

  return $fpm

}

# ------------------------------------------------------------------------------
proc ::prdoc::Draw { {geom ""} } {

  variable CONF
  variable MODKEY
  variable ADDTAG_BINDS
  variable ADDCHAR_BINDS
  variable LAYOUT
  variable DESC_T
  variable Dchanged
  variable geomRE

  if {[winfo exists .pdocu]} {
    wm deiconify .pdocu
  } else {

    set dwid [expr int([winfo screenwidth .]*4.2/[font measure "TkTextFont" "1234567890"])]
    if {[winfo screenheight .] > 800 || $CONF(ShowResTab)} {
      set dhei 25
    } else {
      set dhei 10
    }

    toplevel .pdocu
    wm attributes .pdocu -alpha 0.0

# Program info
    set fpd .pdocu.description
    ttk::labelframe $fpd -relief groove -borderwidth 2 -text " [mc pdocu.description] "
    ttk::label $fpd.title_lbl -text [mc pdocu.prgmtitle] -anchor w
    ttk::entry $fpd.title -textvariable ::prdoc::DESC_T(T)
    ttk::label $fpd.usage_lbl -text [mc pdocu.usage] -anchor w
    grid $fpd.title_lbl -row 0 -column 0 -padx 10 -sticky w
    grid $fpd.title -row 1 -column 0 -padx 10 -sticky we
    grid $fpd.usage_lbl -row 2 -column 0 -padx 10 -sticky w

# Edit frame
    set efrm $fpd.edit
    ttk::frame $efrm

    text $efrm.txt -width $dwid -height $dhei -font TkTextFont -wrap word \
      -undo true -yscrollcommand [list $efrm.ysb set]
    ttk::scrollbar $efrm.ysb -orient vertical -command [list $efrm.txt yview]

    grid $efrm.txt -row 0 -column 0 -sticky nwse -columnspan 2
    grid $efrm.ysb -row 0 -column 2 -sticky ns

    ttk::frame $efrm.tags
    ttk::button $efrm.tags.bold -text [mc gen.boldchar] -style strong.TButton \
      -command "::prdoc::AddTag $efrm.txt strong"
    ttk::button $efrm.tags.italic -text [mc gen.italicchar] -style em.TButton \
      -command "::prdoc::AddTag $efrm.txt em"
    ttk::button $efrm.tags.pre -text "pre" -style pre.TButton \
      -command "::prdoc::AddTag $efrm.txt pre"
    ttk::label $efrm.tags.sep1 -text " "

    ttk::button $efrm.tags.sup -text "sup" -style tag.TButton \
      -command "::prdoc::AddTag $efrm.txt sup"
    ttk::button $efrm.tags.sub -text "sub" -style tag.TButton \
      -command "::prdoc::AddTag $efrm.txt sub"
    ttk::label $efrm.tags.sep2 -text " "

    ttk::button $efrm.tags.ul -text "ul" -style tag.TButton \
      -command "::prdoc::AddTag $efrm.txt ul"
    ttk::button $efrm.tags.ol -text "ol" -style tag.TButton \
      -command "::prdoc::AddTag $efrm.txt ol"
    ttk::button $efrm.tags.li -text "li" -style tag.TButton \
      -command "::prdoc::AddTag $efrm.txt li"
    ttk::label $efrm.tags.sep3 -text " "

    ttk::button $efrm.tags.reg -text "X" -style reg.TButton \
      -command "::prdoc::AddTag $efrm.txt register"
    ttk::button $efrm.tags.keylbl -text "123" -style strong.TButton \
      -width 4 -command "::prdoc::AddTag $efrm.txt KeyLabel"
    ttk::button $efrm.tags.fkeylbl -text "FFF" -style gold.TButton \
      -command "::prdoc::AddTag $efrm.txt fKeyLabel"
    ttk::button $efrm.tags.gkeylbl -text "GGG" -style blue.TButton \
      -command "::prdoc::AddTag $efrm.txt gKeyLabel"
    ttk::button $efrm.tags.f -text "f" -style fkey.TButton \
      -command "::prdoc::AddTag $efrm.txt fKey"
    ttk::button $efrm.tags.g -text "g" -style gkey.TButton \
      -command "::prdoc::AddTag $efrm.txt gKey"
    ttk::label $efrm.tags.sep4 -text " "
    ttk::menubutton $efrm.tags.entities -text [mc gen.symbols] \
      -menu $efrm.tags.entities.em
    prdoc::SymbolsMenu $efrm.tags.entities.em $efrm.txt

    grid $efrm.tags.bold -row 0 -column 0 -sticky ns
    grid $efrm.tags.italic -row 0 -column 1 -sticky ns
    grid $efrm.tags.pre -row 0 -column 2 -sticky ns
    grid $efrm.tags.sep1 -row 0 -column 3 -sticky ns
    grid $efrm.tags.sup -row 0 -column 4 -sticky ns
    grid $efrm.tags.sub -row 0 -column 5 -sticky ns
    grid $efrm.tags.sep2 -row 0 -column 6 -sticky ns
    grid $efrm.tags.ul -row 0 -column 7 -sticky ns
    grid $efrm.tags.ol -row 0 -column 8 -sticky ns
    grid $efrm.tags.li -row 0 -column 9 -sticky ns
    grid $efrm.tags.sep3 -row 0 -column 10 -sticky ns
    grid $efrm.tags.reg -row 0 -column 11 -sticky ns
    grid $efrm.tags.keylbl -row 0 -column 12 -sticky ns
    grid $efrm.tags.fkeylbl -row 0 -column 13 -sticky ns
    grid $efrm.tags.gkeylbl -row 0 -column 14 -sticky ns
    grid $efrm.tags.f -row 0 -column 15 -sticky ns
    grid $efrm.tags.g -row 0 -column 16 -sticky ns
    grid $efrm.tags.sep4 -row 0 -column 17 -sticky ns
    grid $efrm.tags.entities -row 0 -column 18 -sticky ns

    grid $efrm.tags -row 1 -column 0 -sticky nwe -columnspan 2

    ttk::button $efrm.mode -text [mc pdocu.preview] \
      -command "::prdoc::SetMode render"
    grid $efrm.mode -row 1 -column 1 -sticky ne

    ttk::frame $efrm.filler -height 5
    grid $efrm.filler -row 2 -column 1 -sticky we

    grid $efrm -row 3 -column 0 -padx 10 -sticky nwse
    grid columnconfigure $efrm 0 -weight 1
    grid rowconfigure $efrm 0 -weight 1

# Frame for rendering. A separate widget makes it easier to preserve the
# editing widget status (history, cursor, modified, etc.)
    set rfrm $fpd.render
    ttk::frame $rfrm

    text $rfrm.txt -width $dwid -height $dhei -font TkTextFont -wrap word \
      -undo false -yscrollcommand [list $rfrm.ysb set]
    ttk::scrollbar $rfrm.ysb -orient vertical \
      -command [list $rfrm.txt yview]

    grid $rfrm.txt -row 0 -column 0 -sticky nwse -columnspan 2
    grid $rfrm.ysb -row 0 -column 2 -sticky ns

    ttk::button $rfrm.mode -text [mc gen.edit] -command "::prdoc::SetMode edit"
    grid $rfrm.mode -row 1 -column 1 -sticky ne

    ttk::frame $rfrm.filler -height 5
    grid $rfrm.filler -row 2 -column 1 -sticky we

    grid $rfrm -row 3 -column 0 -padx 10 -sticky nwse
    grid columnconfigure $rfrm 0 -weight 1
    grid rowconfigure $rfrm 0 -weight 1
    lower $rfrm

    grid $fpd -row 0 -column 0 -sticky nsew -padx 3
    grid columnconfigure $fpd 0 -weight 1
    grid rowconfigure $fpd 3 -weight 1

    if {![info exists DESC_T(D)] || $DESC_T(D) eq ""} {
      set authorship [string trim [string map \
        {"<" "&lt;" ">" "&gt;" "\u00a9" "&copy;" "&" "&amp;" "\"" "&quot;"} \
        $::HP15(authorship)]]
      set DESC_T(D) [regsub {(https?://|www\.)[\w\.\-~/]+} $authorship \
        {<a href="\0">\0</a>}]
    }

# Fill existing program description and set edit or preview mode
    if {[info exists DESC_T(D)]} {
      $efrm.txt replace 0.0 end $DESC_T(D)
      $efrm.txt edit reset
      if {$CONF(autopreview) && [regexp {</[[:alnum:]]+>|&[[:alpha:]]+;} $DESC_T(D)]} {
        {*}[$efrm.mode cget -command]
      } else {
        {*}[$rfrm.mode cget -command]
      }

    }
    set Dchanged 0

# Add Marks subarea
    DrawMarks .pdocu

# Button frame
    set bfrm .pdocu.btn
    ttk::frame $bfrm -relief flat -borderwidth 8
    ttk::checkbutton $bfrm.restab -text [mc pdocu.restab] \
      -variable ::prdoc::CONF(ShowResTab) -command "::prdoc::DrawMarks .pdocu"
    ttk::button $bfrm.reload -text [mc pdocu.reload] -command "::prdoc::Reload"
    ttk::button $bfrm.ok -text [mc gen.ok] -command "::prdoc::Act yes" \
      -default active
    ttk::button $bfrm.cancel -text [mc gen.cancel] -command "::prdoc::Act no"

    grid $bfrm.restab -row 0 -column 0 -padx 5 -sticky w
    grid $bfrm.reload -row 0 -column 1 -padx 20 -sticky e
    grid $bfrm.ok -row 0 -column 2 -padx 5 -sticky e
    grid $bfrm.cancel -row 0 -column 3 -padx 5 -sticky e
    grid $bfrm -row 2 -column 0 -sticky nsew
    grid columnconfigure $bfrm 0 -weight 1

    grid columnconfigure .pdocu 0 -weight 1
    grid rowconfigure .pdocu 0 -weight 1

    if {$::HP15(prgmname) ne ""} {
      wm title .pdocu "$::APPDATA(appname) [mc gen.program]: $::HP15(prgmname)"
    } else {
      wm title .pdocu "$::APPDATA(appname) [mc gen.program]: [mc pdocu.notsaved]"
    }

    wm minsize .pdocu [expr int([winfo width .pdocu]*0.67)] \
      [expr int([winfo height .pdocu]*0.85)]

# Bindings and tracking changes on description
    set wid $efrm.txt
    $wid edit modified 0
    update
    bind $wid <<Modified>> "::prdoc::Modified %W"
    bind $wid <$MODKEY(ctrl)-v> "::prdoc::ResetModified %W"
    bind $wid <$MODKEY(ctrl)-z> "::prdoc::ResetModified %W"
    bind $wid <$MODKEY(ctrl)-a> {%W tag add sel 1.0 end; break;}
    foreach tt [array names ADDTAG_BINDS] {
      lassign $ADDTAG_BINDS($tt) mk1 mk2 cc
      bind $wid <$MODKEY($mk1)-$MODKEY($mk2)-$cc> "::prdoc::AddTag $wid $tt"
    }
    foreach tt [array names ADDCHAR_BINDS] {
      lassign $ADDCHAR_BINDS($tt) mk1 kk cc
      bind $wid <$MODKEY($mk1)-$kk> "::prdoc::AddChar $wid $cc"
    }
    bind $wid <F5> "::prdoc::HighlightTags $efrm.txt"

    bind .pdocu <Return> "::prdoc::Return %W"
    bind .pdocu <Escape> "$bfrm.cancel invoke"

    if {[trace info variable prdoc::DESC_T] eq ""} {
      trace add variable prdoc::DESC_T write "::prdoc::Changed $wid"
    }

    wm protocol .pdocu WM_DELETE_WINDOW "::prdoc::Act no"

# Recover previous window position if available
    if {$geom ne ""} {
      regexp $geomRE $geom all w0xh0 w0 h0 xoff x0 yoff y0
      regexp $geomRE [winfo geometry .pdocu] all w1xh1 w1 h1 xoff x1 yoff y1

      if {$x0+$w1 > [winfo vrootwidth .]} {
        set x0 [expr [winfo vrootwidth .]-$w1-10]
      }
      if {$x0 < [winfo vrootx .]} { set x0 [expr [winfo vrootx .]+10] }

      if {$y0+$h1 > [winfo vrootheight .]} {
        set y0 [expr [winfo vrootheight .]-$h1-10]
      }
      if {$y0 < [winfo vrooty .]} { set y0 [expr [winfo vrooty .]+10] }

      wm geometry .pdocu [format "+%d+%d" $x0 $y0]
      update
    }

    wm attributes .pdocu -alpha 1.0
    raise .pdocu
    focus $efrm.txt

  }

}

# ------------------------------------------------------------------------------
proc ::prdoc::Edit { {geom ""} } {

  variable prevpos
  variable DESC
  variable DESC_T

  array unset DESC_T
  array set DESC_T [array get DESC]

  if {$geom eq "" && $prevpos ne ""} {
    set geom $prevpos
  }

  Analyse $::PRGM
  Draw $geom

}
